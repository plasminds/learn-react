import { Component } from "react";

class Task extends Component {
    generateTasks() {
        // dans une liste il est important que les enfants soient identifiés par une clé
        // cela permet à React, en interne, de ne pas recharger entièrement la liste en cas de modification
        return this.props.tasks.map((item, k) => <li key={k}>{item}</li>);
    }

    render() {
        return <ul>{this.generateTasks()}</ul>;
    }
}

export default Task;
