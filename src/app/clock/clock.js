import { Component } from "react";

// altération de l'état d'un composant
class Clock extends Component {

    static defaultProps = {
        start: 0,
        step: 1
    };

    constructor(props) {
        super(props);
        this.state = { date: new Date(), i: props.start };
        this.timer = null;
    }

    componentDidMount() {
        this.timer = window.setInterval(this.tick.bind(this), 1000);
    }

    componentWillUnmount() {
        window.clearInterval(this.timer);
    }

    // utiliser une fonction comme argument est plus sûr si le nouvel état dépend d'un état précédent
    tick() {
        this.setState((state, props) => ({ date: new Date(), i: state.i + props.step }));
    }

    render() {
        return <span>{this.state.date.toLocaleDateString()} {this.state.date.toLocaleTimeString()} {this.state.i}</span>;
    }
}

export default Clock;
