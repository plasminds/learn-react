import logo from '../logo.svg';
import './app.css';
import Task from './task/task';
import { Component } from "react";
import Clock from './clock/clock';

let n = 99;

class App extends Component {
    render() {
        return (
            <>
                <header className="app-header">
                    <img src={logo} className="app-logo" alt="logo"/>
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <p id={"bjr_" + n}>Bonjour, il est : <Clock/></p>
                    <Task tasks={['thomas', 'boffy']}/>
                    <a
                        className="app-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </>
        );
        // <> ou <React.Fragment> indique à React de ne pas créer d'élément à la racine du composant
    }
}

export default App;
